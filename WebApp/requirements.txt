Flask==2.0.1
Flask-WTF==0.15.1
json5==0.9.6
matplotlib==3.4.3
pandas==1.3.2
