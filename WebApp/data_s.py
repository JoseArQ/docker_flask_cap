#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 11:23:52 2021


@author: orc
"""

from statistics import stdev, mean

class Data():
    """
    This class performs the statistical calculations and creates histogram

    Methods
    -------
    resum_data()
        Calculate basic statistics for a column of dataframe 

    hist_data()
        Create and save histogram
    """
    def __init__(self, df, column: str ="sepal_length", bins: int =16):
        '''
        Init variables

        Parameters
        ----------
        df : dataframe
            Is a two-dimensional data structure with labeled axes.
        column : str, optional
            Name column of iris dataset for analyze. 
            The default value is "sepal_length".
        bins : int, optional
            Number of histogram bins to be used. 
            The default value is 16.

        Returns
        -------
        None.

        '''
        self.df = df
        self.column = column
        self.bins = bins

    def resum_data(self):
        '''
        Calculate basic statistics for a column of a dataframe

        Returns
        -------
        max_f : float
            Return the maximum of the values over the requested column.
        min_f : float
            Return the minimum of the values over the requested column.
        sd_f : float
            Standard deviation over request column.
        mean_f : float
            Mean over request column.

        '''
        df = self.df
        column = self.column

        max_f = df[column].max()
        min_f = df[column].min()
        sd_f = round(stdev(df[column]), 1)
        mean_f = round(mean(df[column]), 1)

        return max_f, min_f, sd_f, mean_f

    def hist_data(self):
        '''
        Plot selected column and save image

        Returns
        -------
        None.

        '''
        df = self.df
        column = self.column
        bins = self.bins

        df_col = df[column]
        ax = df_col.plot.hist(bins=bins,
                              alpha=0.7,
                              title=column,
                              legend=True,
                              figsize=(8, 6))
        fig = ax.get_figure()
        fig.savefig("static/plot.jpg")
