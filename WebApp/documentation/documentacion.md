# Documentación

Aplicación web que permite analizar los datos de las columnas del conjunto de datos iris puedes
mayor información de los datos en el siguiente link <https://es.wikipedia.org/wiki/Conjunto_de_datos_flor_iris>

***
## Requerimientos

- Git bash [Git bash download](https://git-scm.com/downloads): Se debe iniciar sesión con una 
cuenta de gitlab o github cómo se indica en la siguiente documentación <https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup>

- Docker que puedes descargar de la página ofical [Docker](https://www.docker.com/get-started) 
en sistemas operativos windows se requiere de instalar un subsitema de Linux para windows **wsl**
este subsistema lo puedes instalar usando la consola de comandos de windows o cmd mediante el 
siguiente comando: 

   * `wsl --install`

En el siguiente link se encuentra la documentación de wsl provista por
microsoft [documentación-wsl](https://docs.microsoft.com/en-us/windows/wsl/)  
 
***
# Get started

Para iniciar la aplicación se debe tener previamente activado docker.

1. Clonar el repositorio de la aplicación que se encuentra en [repositorio](https://gitlab.com/JoseArQ/docker_flask_cap.git)
para ello deberá crear una carpeta y abrir desde allí el cmd y escribir o copiar el siguiente
comando:

   * `git clone https://gitlab.com/JoseArQ/docker_flask_cap.git`

Finalizada la clonación tendrá una copia del proyecto **docker_flask_cap** con todos
los archivos y carpetas necesarias.

![estructura del proyecto](images/estructura.png "proyecto clonado")

2. Generar la imagen docker de la aplicación: 
    1. Abrir la consola de comandos o terminal cmd en la ubicación 
       de la carpeta del proyecto **docker_flask_cap**.
    
    2. Crear el contenedor digitando en la consola la siguiente linea: 
        
       * `docker build -t iris_app_docker/uwsgi .`
        
        mediante la opción *-t* se le da nombre a la imagen como iris_app_docker y se le esta pasando como parámetro el archivo uwsgi que es donde se encuentra la información del puerto del contenedor y 
        que está ubicado en el mismo directorio.
        Usando el comando `docker images` se puede ver la lista de las imágenes creadas.

         ![listando imágenes docker](images/DockerImages.png "Listado imágenes")
    
    3. Para desplegar la imigen en un contenedor local se emplea el siguiente comando:

         * `docker run -u root -d -p 8080:8000 --restart unless-stopped -v /WebApp:/WebApp iris_app_docker/uwsgi`
      
      Ampliación del comando anterior:

      - docker run ejecuta un comando en el nuevo contenedor.
      - -u para ingresar el nombre de usuario en este caso root.
      - -d ejecuta el contenedor en segundo plano.
      - -p mapea el puerto de la aplicación que es el 8000 al puerto del contenedor que será el 8080.
      - --restart política de reinicio será unless-stopped que hará que se reinicie el contenedor a menos que se detenga explicitamente. 
      - -v monta el directorio de trabajo actual en el contenedor.

      ![Despliegue de imágen](images/DespliegueImagenDocker.png "Despliegue")

      Para leer más sobre los comandos de Docker puedes ir al siguiente enlace web <https://docs.docker.com/engine/reference/commandline/docker/>

      Se puede verificar que se ha levantado el contenedor con `docker ps` el cual retorna la 
      lista de los contenedores activos.

      ![Lista contenedores](images/ListarContenedores.png "Listado de contenedores")

      Parar detener el contendor usar el comando `docker stop Id` donde Id es el identificador del 
      contenedor creado que puedes obtener listando los contenedores con `docker ps`.

      ![Detener contenedor](images/DetenerContenedor.png "Deteniendo contenedor")
      
    4. Abrir el navegador y dirigirse al localhost:8080 

***   
# Tutorial

En un navegador web dirigirse a la dirección <http://localhost:8080/> allí encontrarás un formulario
con dos campos, el primero es una lista desplegable con las columnas del conjunto de datos iris, y 
la segunda es el campo bins el cuál debe ser un entero y es el número de niveles usados para 
realizar la gráfica del histograma.

![vista principal](images/VistaPrincipalApp.png "Vista principal de la página")

Al dar click en el botón de *Enviar* la aplicación lo redirige a la vista donde se muestran los 
resultados. 

![vista resultados](images/VistaResultados.png "Vista de los resultados")
   
*** 
# Api Doc

## Módulo data_s

Provee una clase para hallar los valores máximo, mínimo, desveción estándar, y media 
de una columna de un dataframe.


### Clases

   - *class* data_s.**Data**\(df, column="sepal_length", bins=16\):

   - Parámetros:


               - df: Dataframe.

                 
               - column: string. 
               Nombre de la columna del dataframe iris a analizar.

               
               - bins: int.
               Cantidad de niveles para gráficar histograma.

 ***
### Métodos

*classmethod* resum_data(): 

- Método para calcular valores máximo, mínimo, desviación estándar, y media 
de una columna de un dataframe. 

- Retorna:


   -tipo: tuple.
  

   Retorna una tupla con los cuatro valores estadísticos:


      - max_f : float

         Valor máximo de los datos de la columna.


      - min_f : float
         Valor mínimo de los datos de la columna.


      - sd_f : float
         Valor de la desviación estándar de los datos de la columna.


      - mean_f : float
         Valor medio de los datos de la columna.
         
   
   *classmethod* hist_data(): 
   
   - Crea la gráfica del histograma con los parámtros inicializado en la clase Data, y guarda 
   la figura en el archivo *static* con el nombre de *plot.jpg*

**Modo de Uso**
   ~~~
   import pandas as pd
   from data_s import Data

   df  = pd.read_csv("iris.csv")

   data_p = Data(df=df) # valores por defecto column = "sepal_length", bins = 16

   max_f, min_f, sd_f, mean_f = data_p.resum_data()

   print(f'max_f = {max_f} min_f = {min_f} sd_f = {sd_f} mean_f = {mean_f}')

   #  max_f = 7.9 min_f = 4.3 sd_f = 0.8 mean_f = 5.8
   
   data_p.hist_data()

   # la figura se guarda en la ruta static/plot.jpg
   ~~~


## Módulo app_flask_front

Aplicación en flask que renderiza dos vistas:

- ruta raíz o página de inicio \('/'\):
   
   renderiza el formulario del template *index.html* con los campos de columnas y bins.

- ruta \('/procesar'\): 
   
   responde a peticiones Post tomando los parámetros column y bins del request.

   Además realiza los cálculos y envía los resultados como contexto al template
   *resultados.html* donde son renderizados.
   

   Para procesar los datos se utiliza el módulo *data_s*, visto anteriormente.

- A continuación se muestra un diagrama general del flujo de la aplicación flask:

<img src="images/flujo.jpeg" height="800" style="margin: auto;">

- Para profundizar sobre la implementación de la aplicación puede consultar la 
documentación de <https://flask.palletsprojects.com/en/2.0.x/api/>

