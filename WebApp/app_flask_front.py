#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 10:21:21 2021

Render applications views, response http requests and run web server.

@author: oscar 
"""


from flask import Flask, render_template, request
from pandas import read_csv
from data_s import Data

app = Flask(__name__)

# load iris dataset
DF = read_csv('iris.csv')

@app.route('/')
def inicio():
    '''
    Render home page and set up column variable 

    Returns
    -------
    str
        String wiht html for render main page.

    '''
    column = DF.columns
    column = column[:-1]

    return render_template("index.html", column=column)

@app.route('/procesar', methods=['POST'])
def procesar():
    '''
    Process request values for render data results 

    Returns
    -------
    str:
        Render html template where showed calculos results.

    '''

    column = request.form.get("column")
    bins = request.form.get("bins")

    data_p = Data(df=DF, column=column, bins=int(bins))

    max_f, min_f, sd_f, mean_f = data_p.resum_data()

    data_p.hist_data()

    return render_template("resultado.html", min_f=min_f,
                            max_f=max_f, sd_f=sd_f,
                            mean_f=mean_f, column=column)


if __name__ == "__main__":
    app.run(port=8000)
