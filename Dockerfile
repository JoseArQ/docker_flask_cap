# Sistema operativo #(primera capa)#
FROM python:3.7.11
LABEL maintainer="Oscar Riojas <oscar@whaleandjaguar.co>"

# Se instala uWSGI y todas las librerias que necesita la aplicacion #(segunda capa)#
COPY WebApp/requirements.txt requirements.txt
RUN pip install uwsgi && pip install -r requirements.txt

# Puerto HTTP por defecto para uWSGI
ARG UWSGI_HTTP_PORT=8000
ENV UWSGI_HTTP_PORT=$UWSGI_HTTP_PORT

# Aplicacion por defecto para uWSGI
ARG UWSGI_APP=app_flask_front
ENV UWSGI_APP=$UWSGI_APP

# Se crea un usuario para arrancar uWSGI
RUN useradd -ms /bin/bash admin
USER admin

# Se copia el contenido de la aplicacion #(tercera capa)#
COPY WebApp /WebAppDocker

# Se copia el fichero con la configuración de uWSGI
COPY uwsgi.ini uwsgi.ini

# Se establece el directorio de trabajo
WORKDIR /WebAppDocker
 
# Se crea un volumen con el contenido de la aplicacion
VOLUME /WebApp
 
# Se inicia uWSGI
ENTRYPOINT ["uwsgi", "--ini", "/uwsgi.ini"]

